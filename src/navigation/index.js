import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import SplashScreen from '../screens/SplashScreen';
import Intro from '../screens/Intro';
import Login from '../screens/Login';
import Home from '../screens/Home';
import Schedule from '../screens/Schedule';
import Profile from '../screens/Profile';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Stack = createStackNavigator();
const Tabs = createMaterialBottomTabNavigator();

const MainApp = () => {
  return (
    <Tabs.Navigator
      shifting={false}
      barStyle={{backgroundColor: '#fff'}}
      activeColor="#4D8AF0"
      inactiveColor="#bbb">
      <Tabs.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({color}) => (
            <Ionicons
              name="home"
              size={20}
              style={{color: color == '#4D8AF0' ? '#4D8AF0' : '#bbb'}}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Schedule"
        component={Schedule}
        options={{
          tabBarIcon: ({color}) => (
            <Ionicons
              name="calendar-sharp"
              size={20}
              style={{color: color == '#4D8AF0' ? '#4D8AF0' : '#bbb'}}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({color}) => (
            <Ionicons
              name="person"
              size={20}
              style={{color: color == '#4D8AF0' ? '#4D8AF0' : '#bbb'}}
            />
          ),
        }}
      />
    </Tabs.Navigator>
  );
};

const Navigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Navigation;
