import AsyncStorage from '@react-native-community/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';
import React, {useContext, useEffect, useState} from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {RootContext} from '../../../App';
import CardEvent from '../../components/CardEvent';

const Home = ({navigation}) => {
  const [tokenJWT, setTokenJWT] = useState(false);
  const [userInfo, setUserInfo] = useState(null);

  const state = useContext(RootContext);

  useEffect(() => {
    getToken();
    getCurrentUser();
  }, []);

  const getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      return token ? setTokenJWT(true) : setTokenJWT(false);
    } catch (error) {
      console.log(error);
    }
  };
  const getLogout = async () => {
    if (userInfo == null) {
      try {
        await AsyncStorage.removeItem('token');
        setTokenJWT(false);
        navigation.replace('Login');
      } catch (err) {
        console.log(err);
      }
    } else {
      try {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        setTokenJWT(false);
        setUserInfo(null);
        state.addData(null);
        navigation.replace('Login');
      } catch (error) {
        console.error(error);
      }
    }
  };

  const getCurrentUser = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      setUserInfo(userInfo);
      if (userInfo) {
        setTokenJWT(true);
        state.addData({
          uri: userInfo.user.photo,
          nama: userInfo.user.name,
          email: userInfo.user.email,
        });
      } else {
        setTokenJWT(false);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#4D8AF0" />
      <View style={styles.header}>
        <TouchableOpacity
          style={styles.btnLogout}
          onPress={() =>
            tokenJWT == true ? getLogout() : navigation.replace('Login')
          }>
          <Text style={styles.textLogout}>LOG OUT</Text>
        </TouchableOpacity>
        <Text style={styles.title}>Welcome,</Text>
        <Text style={styles.nama}>
          {state.dataUser ? state.dataUser.nama : 'Jhon'}
        </Text>
        <Text style={styles.nim}>421611027</Text>
        <View style={{height: 20}} />
      </View>
      <View style={styles.content}>
        <View style={styles.card}>
          <View style={{alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: '#3F3D56',
                marginBottom: 10,
              }}>
              IPK
            </Text>
            <Text style={{fontSize: 16, color: '#3F3D56'}}>4.0</Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: '#3F3D56',
                marginBottom: 10,
              }}>
              SKS Lulus
            </Text>
            <Text style={{fontSize: 16, color: '#3F3D56'}}>100</Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: '#3F3D56',
                marginBottom: 10,
              }}>
              Total SKS
            </Text>
            <Text style={{fontSize: 16, color: '#3F3D56'}}>44</Text>
          </View>
        </View>
        <View style={{margin: 20}}>
          <Text style={{fontSize: 16, fontWeight: 'bold', color: '#3F3D56'}}>
            Events
          </Text>
          <View style={{borderWidth: 1, borderColor: '#bbb'}} />
        </View>
        <CardEvent
          hari="15-08-2020"
          jam="10.00"
          lokasi="Aula"
          title="React Native"
          pemateri="Muhammad Padri"
        />
        <CardEvent
          hari="23-08-2020"
          jam="10.00"
          lokasi="Aula"
          title="Javascript Fundamental"
          pemateri="Muhammad Padri"
        />
      </View>
    </View>
  );
};

export default Home;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#4D8AF0',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#eee',
  },
  nama: {
    fontWeight: 'bold',
    color: '#eee',
  },
  nim: {
    color: '#ddd',
    fontSize: 11,
  },
  content: {
    backgroundColor: '#eee',
    flex: 2,
  },
  card: {
    marginHorizontal: 20,
    backgroundColor: '#fff',
    height: 100,
    marginTop: -50,
    borderRadius: 30,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  btnLogout: {
    width: 70,
    alignSelf: 'flex-end',
    backgroundColor: 'salmon',
    position: 'absolute',
    top: 20,
    padding: 5,
    borderBottomLeftRadius: 10,
    borderTopLeftRadius: 10,
  },
  textLogout: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#fff',
  },
});
