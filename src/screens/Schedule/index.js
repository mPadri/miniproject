import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CardSchedule from '../../components/CardSchedule';

const Schedule = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Ionicons name="calendar-sharp" size={32} color="#fff" />
        <Text style={styles.titleHeader}>Schedule</Text>
      </View>
      <View
        style={{
          marginTop: 20,
          marginLeft: 20,
        }}>
        <Text style={{fontWeight: 'bold', color: '#3F3D56'}}>Tahun Ajaran</Text>
        <Text style={{color: '#3F3D56', fontSize: 12}}>2020/2021</Text>
      </View>
      <View
        style={{
          marginTop: 20,
          marginLeft: 20,
        }}>
        <Text style={{fontWeight: 'bold', color: '#3F3D56'}}>Semester</Text>
        <Text style={{color: '#3F3D56', fontSize: 12}}>Ganjil</Text>
      </View>
      <CardSchedule
        kodeMatkul="MKPW-12"
        sks="3"
        matkul="Pemrograman Web"
        hari="Senin"
        jam="09.00"
        lokasi="Lab 1"
      />
      <CardSchedule
        kodeMatkul="MKAN-13"
        sks="3"
        matkul="Aplikasi Nirkabel"
        hari="Senin"
        jam="13.00"
        lokasi="Lab 1"
      />
      <CardSchedule
        kodeMatkul="MKKJ-14"
        sks="3"
        matkul="Sistem Keamanan Jaringan"
        hari="Rabu"
        jam="09.00"
        lokasi="Lab Cisco"
      />
    </View>
  );
};

export default Schedule;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#4D8AF0',
    padding: 30,
    flexDirection: 'row',
  },
  titleHeader: {
    fontSize: 24,
    color: '#fff',
    marginLeft: 15,
  },
});
