import AsyncStorage from '@react-native-community/async-storage';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import TouchID from 'react-native-touch-id';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {API} from '../../api';
import Or from '../../components/Or';

const config = {
  title: 'Authentication Required', // Android
  imageColor: '#536DFE', // Android
  imageErrorColor: 'red', // Android
  sensorDescription: 'Touch sensor', // Android
  sensorErrorDescription: 'Failed', // Android
  cancelText: 'Cancel', // Android
  fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
  unifiedErrors: true, // use unified error messages (default false)
  passcodeFallback: true,
};

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (error) {
      console.log(error);
    }
  };
  const getLogin = () => {
    let data = {email, password};

    Axios.post(`${API}/login`, data, {
      timeout: 20000,
    })
      .then((res) => {
        saveToken(res.data.token);
        navigation.replace('MainApp');
      })
      .catch((err) => {
        Alert.alert('Data yang di masukan tidak valid !');
      });
  };

  const loginInWithFingerPrint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        navigation.replace('MainApp');
      })
      .catch((error) => {
        Alert.alert('ID Finger Anda Tidak Valid !');
      });
  };

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '5129955728-ev18qjsuea90qtsem0ncdr9e0hl3q30s.apps.googleusercontent.com',
      offlineAccess: true,
    });
  };

  const signInGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();

      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      navigation.replace('MainApp');
    } catch (error) {
      Alert.alert('Gagal Sigin');
    }
  };
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <Image
        resizeMode="contain"
        source={require('../../assets/logo/logo.png')}
        style={{width: 150, height: 60}}
      />
      <View style={{height: 10}} />
      <View>
        <Text>Username / Email</Text>
        <TextInput
          keyboardType="email-address"
          underlineColorAndroid="#ddd"
          placeholder="Username / Email"
          value={email}
          onChangeText={(value) => setEmail(value)}
        />
      </View>
      <View>
        <Text>Password</Text>
        <TextInput
          secureTextEntry={true}
          underlineColorAndroid="#ddd"
          placeholder="Password"
          value={password}
          onChangeText={(value) => setPassword(value)}
        />
      </View>
      <TouchableOpacity style={styles.btnLogin} onPress={() => getLogin()}>
        <Text style={styles.textBtn}>LOGIN</Text>
      </TouchableOpacity>
      <Or />
      <TouchableOpacity
        style={styles.btnLoginFinger}
        onPress={() => loginInWithFingerPrint()}>
        <Ionicons name="finger-print-sharp" color="#fff" size={18} />
        <Text style={styles.textBtn}>LOGIN WITH FINGER</Text>
      </TouchableOpacity>
      <GoogleSigninButton
        style={{width: 198, height: 55, alignSelf: 'center', marginBottom: 10}}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Dark}
        onPress={() => signInGoogle()}
      />
    </View>
  );
};

export default Login;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  btnLogin: {
    backgroundColor: '#69F0AE',
    margin: 15,
    padding: 10,
    borderRadius: 10,
    elevation: 1,
  },
  textBtn: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
  },
  btnLoginFinger: {
    backgroundColor: '#536DFE',
    margin: 15,
    padding: 10,
    borderRadius: 10,
    elevation: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
