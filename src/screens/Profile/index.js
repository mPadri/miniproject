import storage from '@react-native-firebase/storage';
import React, {useContext, useEffect, useState} from 'react';
import {Alert, Image, Modal, Text, TouchableOpacity, View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {RootContext} from '../../../App';
import CardProfile from '../../components/CardProfile';

const Profile = () => {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);
  const [saveProfile, setSaveProfile] = useState(false);

  const state = useContext(RootContext);

  useEffect(() => {
    setPhoto(state.dataUser);
  }, []);

  const toggleCamera = () => {
    setType(type == 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, bas64: true};

    if (camera) {
      const data = await camera.takePictureAsync(options);

      setPhoto(data);
      setIsVisible(false);
      setSaveProfile(true);
    }
  };

  const uploadPhoto = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`image/${sessionId}`)
      .putFile(uri)
      .then((res) => {
        Alert.alert('Sukses Upload');
        setSaveProfile(false);
      })
      .catch((err) => {
        Alert.alert('Gagal Upload');
      });
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            ref={(ref) => {
              camera = ref;
            }}
            type={type}>
            <View>
              <TouchableOpacity
                style={{
                  backgroundColor: '#fff',
                  width: 50,
                  height: 50,
                  borderRadius: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 15,
                }}
                onPress={() => toggleCamera()}>
                <MaterialCommunityIcons name="rotate-3d-variant" size={25} />
              </TouchableOpacity>
            </View>
            <View style={{flex: 1}} />
            <View
              style={{
                borderWidth: 1,
                borderColor: '#fff',
                width: 160,
                height: 230,
                alignSelf: 'center',
                borderRadius: 50,
              }}
            />
            <View style={{flex: 2}} />
            <View style={{alignItems: 'center', marginBottom: 20}}>
              <TouchableOpacity
                style={{
                  backgroundColor: '#fff',
                  width: 50,
                  height: 50,
                  borderRadius: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 15,
                }}
                onPress={() => takePicture()}>
                <Ionicons name="camera" size={25} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };
  return (
    <>
      <View>
        {renderCamera()}
        <View
          style={{
            backgroundColor: '#4D8AF0',
            height: 200,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={
              photo == null
                ? require('../../assets/image/user.png')
                : {uri: photo.uri}
            }
            style={{width: 100, height: 100, borderRadius: 50}}
          />
          <TouchableOpacity onPress={() => setIsVisible(true)}>
            <Text
              style={{
                fontWeight: 'bold',
                color: 'white',
                fontSize: 16,
                marginTop: 10,
              }}>
              Change Photo
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{alignItems: 'center', marginTop: -20}}>
          <CardProfile
            disabled={saveProfile == false ? true : false}
            onPress={() => uploadPhoto(photo.uri)}
            nama={state.dataUser ? state.dataUser.nama : 'Jhon'}
            email={state.dataUser ? state.dataUser.email : 'jhon@email.com'}
          />
        </View>
      </View>
    </>
  );
};

export default Profile;
