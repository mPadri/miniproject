import React from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';

const slides = [
  {
    key: 'one',
    title: 'Welcome',
    image: require('../../assets/image/welcome.png'),
  },
  {
    key: 'two',
    title: 'Get The Information',
    image: require('../../assets/image/informasi.png'),
  },
  {
    key: 'three',
    title: 'Check Schedule',
    image: require('../../assets/image/schedule.png'),
  },
];

const Intro = ({navigation}) => {
  const _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image style={styles.image} source={item.image} />
      </View>
    );
  };

  const _oneDone = () => {
    navigation.replace('Login');
  };

  const _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };
  const _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <AppIntroSlider
        renderItem={_renderItem}
        data={slides}
        onDone={_oneDone}
        activeDotStyle={{backgroundColor: '#22bcb5'}}
        renderNextButton={_renderNextButton}
        renderDoneButton={_renderDoneButton}
        keyExtractor={(item) => item.key}
      />
    </>
  );
};

export default Intro;
const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 15,
    color: '#191970',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#d4d4d4',
    marginTop: 15,
    textAlign: 'center',
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#4D8AF0',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
  },
});
