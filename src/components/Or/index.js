import React from 'react';
import {Text, View} from 'react-native';

const Or = () => {
  return (
    <View
      style={{
        flexDirection: 'row',
        marginVertical: 5,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View style={{height: 2, backgroundColor: '#d4d4d4', width: 145}} />
      <Text style={{marginHorizontal: 5}}>OR</Text>
      <View
        style={{
          height: 2,
          backgroundColor: '#d4d4d4',
          width: 145,
        }}
      />
    </View>
  );
};

export default Or;
