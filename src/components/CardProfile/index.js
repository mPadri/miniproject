import React from 'react';
import {
  Dimensions,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

const CardProfile = ({onPress, disabled, nama, email}) => {
  return (
    <View
      style={{
        backgroundColor: 'white',
        height: 210,
        width: Dimensions.get('window').width - 30,
        borderRadius: 10,
        elevation: 2,
      }}>
      <View style={{padding: 15}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 15,
          }}>
          <Text>Nama</Text>
          <Text>{nama}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 15,
          }}>
          <Text>NIM</Text>
          <Text>421611027</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Text>Email</Text>
          <Text>{email}</Text>
        </View>
      </View>
      <TouchableOpacity
        disabled={disabled}
        style={styles.btnSimpan}
        onPress={onPress}>
        <Text style={styles.textBtn}>Save Profile</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CardProfile;
const styles = StyleSheet.create({
  btnSimpan: {
    backgroundColor: '#69F0AE',
    margin: 15,
    padding: 10,
    borderRadius: 10,
    elevation: 1,
  },
  textBtn: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
  },
});
