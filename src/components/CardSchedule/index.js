import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const CardSchedule = ({kodeMatkul, matkul, sks, hari, jam, lokasi}) => {
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text>{kodeMatkul}</Text>
          <Text>{sks} sks</Text>
        </View>
        <Text
          style={{
            fontSize: 16,
            marginVertical: 10,
            color: '#3F3D56',
            fontWeight: 'bold',
          }}>
          {matkul}
        </Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Ionicons name="calendar" />
            <Text style={{marginLeft: 5}}>{hari}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Ionicons name="alarm" />
            <Text style={{marginLeft: 5}}>{jam}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Ionicons name="location-sharp" />
            <Text style={{marginLeft: 5}}>{lokasi}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default CardSchedule;
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  card: {
    backgroundColor: '#fff',
    height: 100,
    borderRadius: 10,
    elevation: 2,
    padding: 10,
  },
});
