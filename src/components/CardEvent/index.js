import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const CardEvent = ({title, hari, jam, lokasi, pemateri}) => {
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <Text
          style={{
            fontSize: 16,
            marginBottom: 5,
            color: '#3F3D56',
            fontWeight: 'bold',
          }}>
          {title}
        </Text>
        <Text
          style={{
            fontSize: 12,
            color: '#3F3D56',
            marginBottom: 5,
          }}>
          Pemateri: {pemateri}
        </Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Ionicons name="calendar" />
            <Text style={{marginLeft: 5}}>{hari}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Ionicons name="alarm" />
            <Text style={{marginLeft: 5}}>{jam}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Ionicons name="location-sharp" />
            <Text style={{marginLeft: 5}}>{lokasi}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default CardEvent;
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  card: {
    backgroundColor: '#fff',
    height: 100,
    borderRadius: 10,
    elevation: 2,
    padding: 10,
  },
});
