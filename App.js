import React, {useState, useEffect, createContext} from 'react';
import {StyleSheet} from 'react-native';
import SplashScreen from './src/screens/SplashScreen';
import {NavigationContainer} from '@react-navigation/native';
import Navigation from './src/navigation';
import firebase from '@react-native-firebase/app';
import DefaultPhoto from './src/assets/image/user.png';

const firebaseConfig = {
  apiKey: 'AIzaSyAD9VF2mWA-Qb7fPtsAgfzl7wc5cpl290w',
  authDomain: 'myproject-1fa7b.firebaseapp.com',
  databaseURL: 'https://myproject-1fa7b.firebaseio.com',
  projectId: 'myproject-1fa7b',
  storageBucket: 'myproject-1fa7b.appspot.com',
  messagingSenderId: '5129955728',
  appId: '1:5129955728:web:9a8cc7207bf3f59804eb15',
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const RootContext = createContext();
const Context = () => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      <Navigation />
    </NavigationContainer>
  );
};

const App = () => {
  const [dataUser, setDataUser] = useState(null);

  const addData = (value) => {
    setDataUser(value);
  };
  return (
    <RootContext.Provider value={{dataUser, addData}}>
      <Context />
    </RootContext.Provider>
  );
};

const styles = StyleSheet.create({});

export default App;
